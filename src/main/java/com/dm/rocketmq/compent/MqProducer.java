package com.dm.rocketmq.compent;


import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.dm.rocketmq.config.RocketMQProperties;
import com.dm.rocketmq.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;


/**
 * mq消息生产者
 *
 * @author Colin
 * @date 2023/06/05
 */

public class MqProducer {

    private final static Logger log = LoggerFactory.getLogger(MqProducer.class);

    private final ProducerBean producerBean;
    private final RocketMQProperties rocketMQProperties;

    public MqProducer(ProducerBean producerBean, RocketMQProperties rocketMQProperties) {
        this.producerBean = producerBean;
        this.rocketMQProperties = rocketMQProperties;
    }

    public String sendMsg(String msgKey, String topic, String tag, Object msgObj) {
        return sendMsgWithTtl(msgKey, topic, tag, msgObj, null);
    }

    public String sendMsgWithTtl(String msgKey, String topic, String tag, Object msgObj, Long ttl) {
        String finalTopic = rocketMQProperties.getTopics().get(topic).toString();
        String finalTag = rocketMQProperties.getTags().get(tag).toString();
        Message msg = new Message(
                // Message所属的Topic
                finalTopic,
                // Message Tag 可理解为Gmail中的标签，对消息进行再归类，方便Consumer指定过滤条件在MQ服务器过滤
                finalTag,
                // Message Body 可以是任何二进制形式的数据， MQ不做任何干预
                // 需要Producer与Consumer协商好一致的序列化和反序列化方式
                JsonUtils.toJsonString(msgObj).getBytes());


        msgKey = msgKey == null ? "" : msgKey;
        if (StringUtils.hasText(msgKey)) {
            msg.setKey(msgKey);
        }
        log.info("发送一条消息 到mq,topic:[{}],tag:[{}],消息Key[{}]", finalTopic, finalTag, msgKey);
        if (ttl != null) {
            msg.setStartDeliverTime(ttl);
        }
        SendResult send = producerBean.send(msg);
        return send.getMessageId();
    }
}
