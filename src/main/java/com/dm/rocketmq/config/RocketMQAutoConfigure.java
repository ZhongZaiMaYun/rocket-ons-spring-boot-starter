package com.dm.rocketmq.config;


import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.dm.rocketmq.annotation.RocketListenerAnnotationBeanPostProcessor;
import com.dm.rocketmq.compent.MqProducer;
import com.dm.rocketmq.util.OnsUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
@EnableConfigurationProperties(RocketMQProperties.class)
public class RocketMQAutoConfigure {

    private final RocketMQProperties rocketMQProperties;

    public RocketMQAutoConfigure(RocketMQProperties rocketMQProperties) {
        this.rocketMQProperties = rocketMQProperties;
    }

    @Bean
    @ConditionalOnMissingBean(OnsUtil.class)
    public OnsUtil onsUtil(RocketMQProperties rocketMqProperties) {
        return new OnsUtil(rocketMqProperties);
    }

    @Bean
    @ConditionalOnMissingBean(RocketListenerAnnotationBeanPostProcessor.class)
    public RocketListenerAnnotationBeanPostProcessor rocketListenerAnnotationBeanPostProcessor(RocketMQProperties rocketMqProperties, OnsUtil onsUtil) {
        return new RocketListenerAnnotationBeanPostProcessor(rocketMqProperties, onsUtil);
    }

    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public ProducerBean buildProducer() {
        ProducerBean producer = new ProducerBean();
        producer.setProperties(getMqProperties());
        return producer;
    }

    @Bean
    @ConditionalOnBean(ProducerBean.class)
    public MqProducer mqProducer(RocketMQProperties rocketMqProperties) {
        return new MqProducer(buildProducer(), rocketMqProperties);
    }

    public Properties getMqProperties() {
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.AccessKey, rocketMQProperties.getAccessKey());
        properties.setProperty(PropertyKeyConst.SecretKey, rocketMQProperties.getSecretKey());
        properties.setProperty(PropertyKeyConst.NAMESRV_ADDR, rocketMQProperties.getNameSrvAddr());
        return properties;
    }
}
