package com.dm.rocketmq.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * rocketmq 配置
 *
 * @author Colin
 * @since 2022/11/16
 */
@ConfigurationProperties(prefix = "rocket-mq")
public class RocketMQProperties {


    /**
     * 实例id
     */
    private String instanceId;

    /**
     * 端点
     */
    private String endpoint;

    /**
     * 阿里云身份验证 AccessKeyId，在阿里云用户信息管理控制台获取。
     */
    private String accessKey;
    /**
     * 阿里云身份验证 AccessKeySecret，在阿里云用户信息管理控制台获取。
     */
    private String secretKey;
    /**
     * 设置 TCP 接入域名，进入控制台的实例详情页面的 TCP 协议客户端接入点区域查看。
     */
    private String nameSrvAddr;

    /**
     * 普通消费者配置
     */
    private Consume consume;

    /**
     * 有序消费者配置
     */
    private OrderConsume orderConsume;

    /**
     * 主题
     */
    private Map<String, Object> topics;
    /**
     * 标签
     */
    private Map<String, Object> tags;

    public static class Consume {
        /**
         * 默认配置
         */
        private Default defaultConfig;
        /**
         * 线程数
         */
        private String threadNum;

        public Default getDefaultConfig() {
            return defaultConfig;
        }

        public void setDefaultConfig(Default defaultConfig) {
            this.defaultConfig = defaultConfig;
        }

        public String getThreadNum() {
            return threadNum;
        }

        public void setThreadNum(String threadNum) {
            this.threadNum = threadNum;
        }
    }


    public static class OrderConsume {
        /**
         * 默认配置
         */
        private Default defaultConfig;
        /**
         * 线程数
         */
        private String threadNum;

        public Default getDefaultConfig() {
            return defaultConfig;
        }

        public void setDefaultConfig(Default defaultConfig) {
            this.defaultConfig = defaultConfig;
        }

        public String getThreadNum() {
            return threadNum;
        }

        public void setThreadNum(String threadNum) {
            this.threadNum = threadNum;
        }
    }


    public static class Default {
        /**
         * 消费组ID
         */
        private String groupId;

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getNameSrvAddr() {
        return nameSrvAddr;
    }

    public void setNameSrvAddr(String nameSrvAddr) {
        this.nameSrvAddr = nameSrvAddr;
    }

    public Consume getConsume() {
        return consume;
    }

    public void setConsume(Consume consume) {
        this.consume = consume;
    }

    public OrderConsume getOrderConsume() {
        return orderConsume;
    }

    public void setOrderConsume(OrderConsume orderConsume) {
        this.orderConsume = orderConsume;
    }

    public Map<String, Object> getTopics() {
        return topics;
    }

    public void setTopics(Map<String, Object> topics) {
        this.topics = topics;
    }

    public Map<String, Object> getTags() {
        return tags;
    }

    public void setTags(Map<String, Object> tags) {
        this.tags = tags;
    }
}


