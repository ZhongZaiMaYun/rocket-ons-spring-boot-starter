package com.dm.rocketmq.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * rocketMq应用程序上下文初始化
 *
 * @author Colin
 * @date 2023/02/06
 */
public class RocketMqApplicationContextInitializer implements
        ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final Logger log = LoggerFactory.getLogger(RocketMqApplicationContextInitializer.class);


    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        log.info(" RocketMQ启动---- ");
    }
}
