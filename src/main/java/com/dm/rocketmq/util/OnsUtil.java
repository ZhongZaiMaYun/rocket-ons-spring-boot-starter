package com.dm.rocketmq.util;


import com.aliyun.ons20190214.Client;
import com.aliyun.ons20190214.models.OnsTopicCreateRequest;
import com.aliyun.ons20190214.models.OnsTopicCreateResponse;
import com.aliyun.ons20190214.models.OnsTopicListRequest;
import com.aliyun.ons20190214.models.OnsTopicListResponse;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.dm.rocketmq.config.RocketMQProperties;
import com.dm.rocketmq.exception.RocketMQException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;


public class OnsUtil implements InitializingBean {

    private final static Logger logger = LoggerFactory.getLogger(OnsUtil.class);

    Client client = null;
    RocketMQProperties rocketMQProperties;

    public OnsUtil(RocketMQProperties rocketMQProperties) {
        this.rocketMQProperties = rocketMQProperties;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Config config = new Config()
                //  AccessKey ID
                .setAccessKeyId(rocketMQProperties.getAccessKey())
                //  AccessKey Secret
                .setAccessKeySecret(rocketMQProperties.getSecretKey());
        if (StringUtils.isEmpty(rocketMQProperties.getEndpoint())) {
            logger.error("RocketMQProperties endpoint attribute missing. ");
            throw new RocketMQException("RocketMQProperties endpoint attribute missing. ");
        }

        // 访问的域名
        config.endpoint = rocketMQProperties.getEndpoint().trim();
        client = new Client(config);
    }


    /**
     * 查询topic列表
     */
    public OnsTopicListResponse onsTopicList(String topic, String instanceId) throws Exception {
        OnsTopicListResponse onsTopicList = null;
        OnsTopicListRequest onsTopicListRequest = new OnsTopicListRequest();
        onsTopicListRequest.setTopic(topic);
        onsTopicListRequest.setInstanceId(instanceId);
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            onsTopicList = client.onsTopicListWithOptions(onsTopicListRequest, runtime);
        } catch (TeaException error) {
            logger.error("TeaException", error);
            throw error;
        } catch (Exception _error) {
            logger.error("Exception", _error);
            throw _error;
        }
        return onsTopicList;
    }

    /**
     * 创建topic
     */
    public OnsTopicCreateResponse onsTopicCreate(String instanceId, String topic, Integer messageType, String remark) throws Exception {
        OnsTopicCreateResponse topicCreateResponse = null;
        OnsTopicCreateRequest onsTopicCreateRequest = new OnsTopicCreateRequest();
        onsTopicCreateRequest.setTopic(topic);
        onsTopicCreateRequest.setInstanceId(instanceId);
        onsTopicCreateRequest.setMessageType(messageType);
        onsTopicCreateRequest.setRemark(remark);
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            topicCreateResponse = client.onsTopicCreateWithOptions(onsTopicCreateRequest, runtime);
        } catch (TeaException error) {
            if (!"BIZ_TOPIC_EXISTED".equals(error.code)) {
                //排除topic 已经创建的情况
                logger.error("TeaException", error);
                throw error;
            }
        } catch (Exception _error) {
            logger.error("Exception", _error);
            throw _error;
        }
        return topicCreateResponse;
    }


}
