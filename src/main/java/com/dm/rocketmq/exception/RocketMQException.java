package com.dm.rocketmq.exception;

/**
 * exception
 *
 * @author Colin
 * @date 2023/04/06
 */
public class RocketMQException extends RuntimeException {
    public RocketMQException() {
    }

    public RocketMQException(String message) {
        super(message);
    }

    public RocketMQException(String message, Throwable cause) {
        super(message, cause);
    }

    public RocketMQException(Throwable cause) {
        super(cause);
    }

    public RocketMQException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
