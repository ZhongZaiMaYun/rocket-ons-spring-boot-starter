package com.dm.rocketmq.container;

import com.aliyun.openservices.ons.api.Admin;
import com.aliyun.openservices.ons.api.Message;
import com.dm.rocketmq.annotation.ConsumeMode;
import com.dm.rocketmq.annotation.RocketListenerAnnotationBeanPostProcessor;
import com.dm.rocketmq.config.RocketMQProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.SmartLifecycle;
import org.springframework.core.env.Environment;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * listener容器
 *
 * @author Colin
 * @date 2023/03/14
 */
public abstract class RocketMQListenerContainer implements EnvironmentAware, DisposableBean, SmartLifecycle {

    private final static Logger log = LoggerFactory.getLogger(RocketMQListenerContainer.class);

    /**
     * 运行状态
     */
    protected boolean running;

    /**
     * 消费者
     */
    protected Admin consumer;

    /**
     * 名字
     */
    protected String name;

    /**
     * 配置信息
     */
    protected RocketMQProperties rocketMQProperties;

    /**
     * 消费模式
     */
    protected ConsumeMode consumeMode;

    /**
     * 消费组
     */
    protected String groupId;

    /**
     * 环境
     */
    protected Environment environment;

    /**
     * 侦听器方法
     */
    protected List<RocketListenerAnnotationBeanPostProcessor.ListenerMethod> listenerMethods;

    /**
     * 执行方法映射
     */
    protected Map<String, BeanMethod> executionMethodMap = new HashMap<>();

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void stop(Runnable callback) {
        stop();
        callback.run();
    }

    @Override
    public int getPhase() {
        // Returning Integer.MAX_VALUE only suggests that
        // we will be the first bean to shutdown and last bean to start
        return Integer.MAX_VALUE;
    }

    @Override
    public void destroy() throws Exception {
        this.setRunning(false);
        if (Objects.nonNull(consumer)) {
            consumer.shutdown();
        }
        log.info("container destroyed, {}", this);
    }

    @Override
    public void start() {
        if (this.isRunning()) {
            throw new IllegalStateException("container already running. " + this);
        }

        try {
            consumer.start();
        } catch (Exception e) {
            throw new IllegalStateException("Failed to start RocketMQ push consumer", e);
        }
        this.setRunning(true);

        log.info("running container: {}", this);
    }

    @Override
    public void stop() {
        if (this.isRunning()) {
            if (Objects.nonNull(consumer)) {
                consumer.shutdown();
            }
            setRunning(false);
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    protected void executionMethod(Message message, Object context, String key, Map<String, BeanMethod> executionMethodMap) throws IllegalAccessException, InvocationTargetException {
        BeanMethod beanMethod = executionMethodMap.get(key);
        Method method = beanMethod.getMethod();
        Object bean = beanMethod.getBean();
        Class<?>[] parameterTypes = method.getParameterTypes();
        String contextName = context.getClass().getName();

        //参数
        Object[] parameters = new Object[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++) {
            Object parameter = null;
            String parameterName = parameterTypes[i].getName();
            switch (parameterName) {
                case "com.aliyun.openservices.ons.api.Message":
                    parameter = message;
                    break;
                case "com.aliyun.openservices.ons.api.ConsumeContext":
                    if ("com.aliyun.openservices.ons.api.ConsumeContext".equals(contextName)) {
                        parameter = context;
                    }
                    break;
                case "com.aliyun.openservices.ons.api.order.ConsumeOrderContext":
                    if ("com.aliyun.openservices.ons.api.order.ConsumeOrderContext".equals(contextName)) {
                        parameter = context;
                    }
                    break;
            }
            parameters[i] = parameter;
        }

        //调用方法
        method.invoke(bean, parameters);
    }

    protected void initBeanMethod(String topic, String tag, Object bean, Method method) {
        String key = environment.resolvePlaceholders(topic) + ";" + environment.resolvePlaceholders(tag);
        BeanMethod beanMethod = new BeanMethod(bean, method);
        executionMethodMap.put(key, beanMethod);
        log.debug("=====> executionMethodMap.put key[{}],value[{}]", key, beanMethod);
    }


    /**
     * bean方法
     *
     * @author Colin
     * @date 2023/03/13
     */
    public static class BeanMethod {
        private Object bean;
        private Method method;

        public BeanMethod() {
        }

        public BeanMethod(Object bean, Method method) {
            this.bean = bean;
            this.method = method;
        }

        public Object getBean() {
            return bean;
        }

        public void setBean(Object bean) {
            this.bean = bean;
        }

        public Method getMethod() {
            return method;
        }

        public void setMethod(Method method) {
            this.method = method;
        }
    }


    /**
     * 验证
     */
    protected abstract void validation();


    public void setRunning(boolean running) {
        this.running = running;
    }

    public Admin getConsumer() {
        return consumer;
    }

    public void setConsumer(Admin consumer) {
        this.consumer = consumer;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RocketMQProperties getRocketMQProperties() {
        return rocketMQProperties;
    }

    public void setRocketMQProperties(RocketMQProperties rocketMQProperties) {
        this.rocketMQProperties = rocketMQProperties;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public ConsumeMode getConsumeMode() {
        return consumeMode;
    }

    public void setConsumeMode(ConsumeMode consumeMode) {
        this.consumeMode = consumeMode;
    }

    public List<RocketListenerAnnotationBeanPostProcessor.ListenerMethod> getListenerMethods() {
        return listenerMethods;
    }

    public void setListenerMethods(List<RocketListenerAnnotationBeanPostProcessor.ListenerMethod> listenerMethods) {
        this.listenerMethods = listenerMethods;
    }
}
