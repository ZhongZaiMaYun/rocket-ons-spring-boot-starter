package com.dm.rocketmq.container.impl;


import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ConsumerBean;
import com.aliyun.openservices.ons.api.bean.Subscription;
import com.aliyun.openservices.shade.com.alibaba.rocketmq.client.exception.MQClientException;
import com.aliyun.openservices.shade.org.apache.commons.lang3.StringUtils;
import com.dm.rocketmq.annotation.RocketListenerAnnotationBeanPostProcessor;
import com.dm.rocketmq.constant.RocketMqConstants;
import com.dm.rocketmq.container.RocketMQListenerContainer;
import com.dm.rocketmq.exception.RocketMQException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 默认rocketMQ执行容器
 *
 * @author Colin
 * @date 2023/03/13
 */
public class DefaultCommonRocketMQListenerContainer extends RocketMQListenerContainer implements InitializingBean {
    private final static Logger log = LoggerFactory.getLogger(DefaultCommonRocketMQListenerContainer.class);


    @Override
    public void afterPropertiesSet() throws Exception {
        initRocketMQPushConsumer();
    }


    private void initRocketMQPushConsumer() throws MQClientException {
        //检验
        validation();

        ConsumerBean consumerBean;
        //创建消费者
        consumerBean = new ConsumerBean();
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.AccessKey, rocketMQProperties.getAccessKey());
        properties.setProperty(PropertyKeyConst.SecretKey, rocketMQProperties.getSecretKey());
        properties.setProperty(PropertyKeyConst.NAMESRV_ADDR, rocketMQProperties.getNameSrvAddr());
        properties.setProperty(PropertyKeyConst.ConsumeThreadNums, rocketMQProperties.getConsume().getThreadNum());
        properties.setProperty(PropertyKeyConst.GROUP_ID, environment.resolvePlaceholders(groupId));
        consumerBean.setProperties(properties);
        //初始化监听方法
        MessageListener messageListener = (message, context) -> {
            String topic = message.getTopic();
            String tag = message.getTag();
            String key = topic + ";" + tag;
            log.debug("=====> {} received an message: key[{}], message 内容 {} ", name, key, message);
            if (executionMethodMap.containsKey(key)) {
                try {
                    executionMethod(message, context, key, executionMethodMap);
                    return Action.CommitMessage;
                } catch (Exception e) {
                    log.error("=====> {} received an message: key[{}], reflex call error ", name, key, e);
                    return Action.ReconsumeLater;
                }
            }
            log.debug("=====> {} received an message: key[{}], executionMethodMap not contains the key ", name, key);
            return Action.ReconsumeLater;
        };


        //订阅关系 按topic 分组
        Map<String, List<RocketListenerAnnotationBeanPostProcessor.ListenerMethod>> topics = listenerMethods.stream().collect(Collectors.groupingBy(listenerMethod -> listenerMethod.getAnnotation().topic()));
        //订阅关系
        Map<Subscription, MessageListener> subscriptionTable = new HashMap<>();
        topics.forEach((topic, listenerMethods) -> {
            Set<String> finalTags = new HashSet<>();
            //获取真实的topic
            String finalTopic = rocketMQProperties.getTopics().get(topic).toString();
            listenerMethods.forEach(listenerMethod -> {
                String[] tags = listenerMethod.getAnnotation().tag();
                Object bean = listenerMethod.getBean();
                Method method = listenerMethod.getMethod();
                for (String tag : tags) {
                    //获取真实的tag
                    String finalTag = rocketMQProperties.getTags().get(tag).toString();
                    if (StringUtils.isBlank(finalTag)) {
                        log.error(String.format("The tag is missing key: '%s'. ", tag));
                        throw new RocketMQException(String.format("The tag is missing key: '%s'. ", tag));
                    }
                    finalTags.add(finalTag);
                    initBeanMethod(finalTopic, finalTag, bean, method);
                }
            });
            Subscription subscription = new Subscription();
            if (StringUtils.isBlank(finalTopic)) {
                log.error(String.format("The topic is missing key: '%s'. ", topic));
                throw new RocketMQException(String.format("The topic is missing key: '%s'. ", topic));
            }

            String tag = StringUtils.join(finalTags, " || ");
            subscription.setTopic(finalTopic);
            subscription.setExpression(tag);
            log.debug("=====> {} init subscription : topic[{}], tag[{}]", name, finalTopic, tag);
            subscriptionTable.put(subscription, messageListener);
        });
        //设置消费者
        consumerBean.setSubscriptionTable(subscriptionTable);
        setConsumer(consumerBean);
    }

    @Override
    protected void validation() {
        if (getConsumer() != null) {
            throw new IllegalArgumentException("Property 'rocketMQListener' or 'rocketMQReplyListener' is required");
        }
        //验证分组ID
        if (RocketMqConstants.DEFAULT_GROUP.equals(groupId)) {
            if (StringUtils.isBlank(groupId) || rocketMQProperties.getConsume() == null || rocketMQProperties.getConsume().getDefaultConfig() == null || StringUtils.isBlank(rocketMQProperties.getConsume().getDefaultConfig().getGroupId())) {
                throw new IllegalArgumentException(String.format(" Consume groupId information is missing, please configuration '%s'}.", RocketMqConstants.CONSUME_DEFAULT_GROUP));
            }
            setGroupId(rocketMQProperties.getConsume().getDefaultConfig().getGroupId());
        } else if (StringUtils.isBlank(groupId) || StringUtils.isBlank(environment.resolvePlaceholders(groupId))) {
            throw new IllegalArgumentException(String.format(" Consume groupId information is missing, please configuration '%s' or @RocketListener attribute groupId", RocketMqConstants.CONSUME_DEFAULT_GROUP));
        }
    }
}
