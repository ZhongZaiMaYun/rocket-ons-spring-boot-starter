package com.dm.rocketmq.annotation;

import com.aliyun.ons20190214.models.OnsTopicListResponse;
import com.aliyun.ons20190214.models.OnsTopicListResponseBody;
import com.dm.rocketmq.config.RocketMQProperties;
import com.dm.rocketmq.constant.RocketMqConstants;
import com.dm.rocketmq.container.RocketMQListenerContainer;
import com.dm.rocketmq.container.impl.DefaultCommonRocketMQListenerContainer;
import com.dm.rocketmq.container.impl.DefaultOrderRocketMQListenerContainer;
import com.dm.rocketmq.exception.RocketMQException;
import com.dm.rocketmq.util.OnsUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * RocketListener 注解 bean 后处理器
 *
 * @author Colin
 * @date 2023/03/08
 */
public class RocketListenerAnnotationBeanPostProcessor implements BeanPostProcessor, EnvironmentAware, Ordered, ApplicationContextAware, SmartInitializingSingleton {

    /**
     * 日志记录器
     */
    private final Logger logger = LoggerFactory.getLogger(RocketListenerAnnotationBeanPostProcessor.class);

    /**
     * 应用程序上下文
     */
    private ConfigurableApplicationContext applicationContext;


    /**
     * 计数器
     */
    private final AtomicLong counter = new AtomicLong(0);

    /**
     * 环境
     */
    private Environment environment;


    /**
     * mq 配置文件
     */
    private final RocketMQProperties rocketMQProperties;

    /**
     * ons 工具
     */
    private final OnsUtil onsUtil;


    /**
     * 类型缓存
     */
    private final ConcurrentMap<Class<?>, TypeMetadata> typeCache = new ConcurrentHashMap<>();

    public RocketListenerAnnotationBeanPostProcessor(RocketMQProperties rocketMQProperties, OnsUtil onsUtil) {
        this.rocketMQProperties = rocketMQProperties;
        this.onsUtil = onsUtil;
    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = (ConfigurableApplicationContext) applicationContext;
    }

    /**
     * bean 初始化后执行
     *
     * @param bean     bean
     * @param beanName bean名字
     * @return {@link Object}
     * @throws BeansException 异常
     */
    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        // 找到bean 的原始类
        Class<?> targetClass = AopUtils.getTargetClass(bean);
        // 元数据有缓存 从缓存取  没有缓存 建立建立元数据
        this.typeCache.computeIfAbsent(targetClass, aClass -> buildMetadata(targetClass, bean));
        return bean;
    }


    /**
     * 单例实例化后
     */
    @Override
    public void afterSingletonsInstantiated() {
        synchronized (this.getClass()) {
            //解析所有
            List<ListenerMethod> allListenerMethod = new ArrayList<>();
            typeCache.values().stream().forEach(typeMetadata -> {
                if (typeMetadata != TypeMetadata.EMPTY) allListenerMethod.addAll(typeMetadata.getListenerMethods());
            });
            //过滤掉不需要创建的topic
            List<ListenerMethod> topicInfos = allListenerMethod.stream().filter(listenerMethod -> {
                if (listenerMethod.getAnnotation().topicInfo() != null &&
                        listenerMethod.getAnnotation().topicInfo().length > 0) {
                    TopicInfo topicInfo = listenerMethod.getAnnotation().topicInfo()[0];
                    return topicInfo.created();
                }
                return false;
            }).collect(Collectors.toList());
            //过滤掉不需要订阅的topic
            List<ListenerMethod> methods = allListenerMethod.stream().filter(listenerMethod -> listenerMethod.getAnnotation().subscribe()).collect(Collectors.toList());
            //开始创建Topic
            //processCreateTopic(topicInfos);
            //开始生成监听者
            processListener(methods);
        }

    }

    private void processCreateTopic(List<ListenerMethod> topicInfos) {
        //根据Topic分组
        Map<String, List<ListenerMethod>> topicListMap = topicInfos.stream().collect(Collectors.groupingBy(listenerMethod -> listenerMethod.getAnnotation().topic()));
        topicListMap.forEach((topic, listMethods) -> {
            Map<TopicMessageType, List<ListenerMethod>> listMap = listMethods.stream().collect(Collectors.groupingBy(listenerMethod -> listenerMethod.getAnnotation().topicInfo()[0].messageType()));
            if (listMap.size() != 1) {
                logger.error("The topic of an instance can only define one message type. ");
                throw new RocketMQException("The topic of an instance can only define one message type. ");
            }
            //开始创建
            String finalTopic = environment.resolvePlaceholders(topic);
            String finalInstanceId = rocketMQProperties.getInstanceId();
            if (!StringUtils.hasText(finalTopic)) {
                logger.error(String.format("The topic is missing key: '%s'. ", topic));
                throw new RocketMQException(String.format("The topic is missing key: '%s'. ", topic));
            }
            if (!StringUtils.hasText(finalInstanceId)) {
                logger.error(String.format("The instanceId is missing key: '%s'. ", RocketMqConstants.DEFAULT_INSTANCE_ID));
                throw new RocketMQException(String.format("The instanceId is missing key: '%s'. ", RocketMqConstants.DEFAULT_INSTANCE_ID));
            }

            OnsTopicListResponse onsTopicListResponse = null;
            List<ListenerMethod> methods = listMap.values().stream().findFirst().get();
            TopicInfo topicInfo = methods.get(0).getAnnotation().topicInfo()[0];
            try {
                logger.debug("正在请求云端Topic数据,Topic {}, 实例ID {}", finalTopic, finalInstanceId);
                //先查询topic
                onsTopicListResponse = onsUtil.onsTopicList(finalTopic, finalInstanceId);
                logger.debug("云端Topic数据,Topic {}, 请求结果 {}", finalTopic, onsTopicListResponse);
            } catch (Exception e) {
                throw new RocketMQException(e);
            }
            if (onsTopicListResponse.getBody().data.publishInfoDo == null || onsTopicListResponse.getBody().data.publishInfoDo.isEmpty()) {
                try {
                    //没有该topic 开始创建
                    logger.debug("云端Topic数据,Topic {} 未创建, 实例ID {},开始创建Topic，消息类型：{}-{}", finalTopic, finalInstanceId, topicInfo.messageType().getType(), topicInfo.messageType().getDesc());
                    onsUtil.onsTopicCreate(finalInstanceId, finalTopic, topicInfo.messageType().getType(), topicInfo.remark());
                    logger.debug("Topic {}, 实例ID {} ,创建Topic成功", finalTopic, finalInstanceId);
                } catch (Exception e) {
                    throw new RocketMQException(e);
                }
            } else {
                //云端 含有此topic 检查定义是否冲突
                OnsTopicListResponseBody.OnsTopicListResponseBodyDataPublishInfoDo dataPublishInfoDo = onsTopicListResponse.getBody().data.publishInfoDo.get(0);
                if (!Objects.equals(dataPublishInfoDo.messageType, topicInfo.messageType().getType())) {
                    String errorStr = String.format("The messageType of the defined topic in the application already conflicts with the topic in the cloud. Please check the configuration topic ['%s'].", finalTopic);
                    logger.error(errorStr);
                    throw new RocketMQException(errorStr);
                }
            }

        });

    }

    private void processListener(List<ListenerMethod> allListenerMethod) {

        //根据消费模式生成消费者
        Map<ConsumeMode, List<ListenerMethod>> consumeModeListMap = allListenerMethod.stream().collect(Collectors.groupingBy(listenerMethod -> listenerMethod.getAnnotation().consumeMode()));
        GenericApplicationContext genericApplicationContext = (GenericApplicationContext) applicationContext;
        consumeModeListMap.forEach((mode, listMethods) -> {
            /**
             * 根据groupId 再次分组  想实现的情况是： 同一个groupId 在一个消费模式下 只有一个消费者
             * 例如： @RocketListener( topic = "topic1", tag ="tag1" , groupId = "group1" , consumeMode = ConsumeMode.CONCURRENTLY);
             *       @RocketListener( topic = "topic2", tag ="tag2" , groupId = "group1" , consumeMode = ConsumeMode.CONCURRENTLY);
             *       @RocketListener( topic = "topic3", tag ="tag3" , groupId = "group1" , consumeMode = ConsumeMode.ORDERLY);
             *       @RocketListener( topic = "topic4", tag ="tag4" , groupId = "group2" , consumeMode = ConsumeMode.CONCURRENTLY);
             *       @RocketListener( topic = "topic5", tag ="tag5" , groupId = "group2" , consumeMode = ConsumeMode.ORDERLY);
             *
             *       这样会产生4个消费者
             *          消费者1 会监听 （topic1,tag1 ） and （topic2,tag2 ）分组在 group1 消费模式是CONCURRENTLY
             *          消费者2 会监听 （topic3,tag3 ） 分组在 group1 消费模式是ORDERLY
             *          消费者3 会监听 （topic4,tag4 ） 分组在 group2 消费模式是CONCURRENTLY
             *          消费者4 会监听 （topic5,tag5 ） 分组在 group2 消费模式是ORDERLY
             */

            Map<String, List<ListenerMethod>> groupIdMap = listMethods.stream().collect(Collectors.groupingBy(listenerMethod -> listenerMethod.getAnnotation().groupId()));
            groupIdMap.forEach((groupId, methods) -> {
                String containerBeanName = String.format("%s_%s", RocketMQListenerContainer.class.getName(), counter.incrementAndGet());
                //根据 消费模式生成消费者容器
                RocketMQListenerContainer rocketMQListenerContainer = createRocketMQListenerContainer(containerBeanName, groupId, mode, methods);
                //动态注册bean 到容器
                genericApplicationContext.registerBean(containerBeanName, RocketMQListenerContainer.class, () -> rocketMQListenerContainer);
                RocketMQListenerContainer container = genericApplicationContext.getBean(containerBeanName, RocketMQListenerContainer.class);
                //开启容器
                if (!container.isRunning()) {
                    try {
                        container.start();
                    } catch (Exception e) {
                        logger.error("Started container failed. ", e);
                        throw new RocketMQException(e);
                    }
                }
                logger.info("Register the listener to container, containerBeanName:{}", containerBeanName);
            });
        });
    }


    /**
     * 创建消费者容器
     *
     * @param containerBeanName 容器bean名称
     * @param consumeMode       消费模式
     * @param listenerMethods   侦听器方法
     * @return {@link RocketMQListenerContainer}
     */
    private RocketMQListenerContainer createRocketMQListenerContainer(String containerBeanName, String groupId, ConsumeMode consumeMode, List<ListenerMethod> listenerMethods) {

        RocketMQListenerContainer container = null;
        switch (consumeMode) {
            case CONCURRENTLY:
                container = new DefaultCommonRocketMQListenerContainer();
                break;
            case ORDERLY:
                container = new DefaultOrderRocketMQListenerContainer();
                break;
        }
        if (container == null) {
            throw new IllegalStateException(String.format("@RocketListener not support this consume mode '%s' ", consumeMode));
        }
        container.setName(containerBeanName);
        container.setRocketMQProperties(rocketMQProperties);
        container.setConsumeMode(consumeMode);
        container.setGroupId(groupId);
        container.setListenerMethods(listenerMethods);
        return container;
    }


    /**
     * 建立元数据
     * RocketListener 注解 与 执行的方法信息
     *
     * @param targetClass 目标类
     * @return {@link TypeMetadata}
     */
    private TypeMetadata buildMetadata(Class<?> targetClass, Object bean) {
        final List<ListenerMethod> methods = new ArrayList<>();
        final List<RocketListener> rocketListeners = new ArrayList<>();

        ReflectionUtils.doWithMethods(targetClass, method -> {
            Collection<RocketListener> listenerAnnotations = findListenerAnnotations(method);
            if (!listenerAnnotations.isEmpty()) {
                RocketListener rocketListener = (RocketListener) listenerAnnotations.toArray()[0];
                methods.add(new ListenerMethod(method, rocketListener, bean));
                rocketListeners.add(rocketListener);
            }
        }, ReflectionUtils.USER_DECLARED_METHODS);
        if (methods.isEmpty()) {
            return TypeMetadata.EMPTY;
        }
        return new TypeMetadata(methods, rocketListeners);
    }

    /**
     * 查找  方法级别RocketListener注解
     *
     * @param element 元素
     * @return {@link Collection}<{@link RocketListener}>
     */
    private Collection<RocketListener> findListenerAnnotations(AnnotatedElement element) {
        return MergedAnnotations.from(element, MergedAnnotations.SearchStrategy.TYPE_HIERARCHY).stream(RocketListener.class).map(ann -> ann.synthesize()).collect(Collectors.toList());
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }


    /**
     * 方法元数据
     *
     * @author Colin
     * @date 2023/03/08
     */
    private static class TypeMetadata {
        /**
         * 类中监听器方法数组
         */
        final List<ListenerMethod> listenerMethods;
        /**
         * 类中监听器方法数组
         */
        final List<RocketListener> rocketListeners;

        static final TypeMetadata EMPTY = new TypeMetadata();

        private TypeMetadata() {
            this.listenerMethods = new ArrayList<>(0);
            this.rocketListeners = new ArrayList<>(0);
        }

        TypeMetadata(List<ListenerMethod> methods, List<RocketListener> rocketListeners) {
            this.listenerMethods = methods;
            this.rocketListeners = rocketListeners;
        }

        public List<ListenerMethod> getListenerMethods() {
            return listenerMethods;
        }

        public List<RocketListener> getRocketListeners() {
            return rocketListeners;
        }
    }


    /**
     * 监听器方法
     *
     * @author Colin
     * @date 2023/03/08
     */
    public static class ListenerMethod {

        /**
         * 方法对象
         */
        final Method method;

        /**
         * RocketListener注解
         */
        final RocketListener annotation;

        /**
         * bean对象 真正执行方法的对象 可能是普通对象 或者 代理对象
         */
        final Object bean;

        ListenerMethod(Method method, RocketListener annotation, Object bean) {
            this.method = method;
            this.annotation = annotation;
            this.bean = bean;
        }

        public Method getMethod() {
            return method;
        }

        public RocketListener getAnnotation() {
            return annotation;
        }

        public Object getBean() {
            return bean;
        }
    }

}