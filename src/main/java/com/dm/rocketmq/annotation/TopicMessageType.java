package com.dm.rocketmq.annotation;


/**
 * topic消息类型
 *
 * @author Colin
 * @date 2023/04/04
 */
public enum TopicMessageType {
    /**
     * 普通消息
     */
    COMMON(0, "普通消息"),

    /**
     * 分区顺序消息
     */
    ZONING_SEQUENCE(1, "分区顺序消息"),

    /**
     * 全局顺序消息
     */
    GLOBAL_SEQUENCE(2, "全局顺序消息"),

    /**
     * 事务消息
     */
    TRANSACTION(4, "事务消息"),

    /**
     * 定时延迟消息
     */
    TIME_DELAY(5, "定时延迟消息"),
    ;

    Integer type;
    String desc;

    TopicMessageType(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
