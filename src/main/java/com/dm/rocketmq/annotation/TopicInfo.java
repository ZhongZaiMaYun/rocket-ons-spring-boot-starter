package com.dm.rocketmq.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * topic信息
 *
 * @author Colin
 * @date 2023/04/04
 */
@Target({})
@Retention(RetentionPolicy.RUNTIME)
public @interface TopicInfo {

    /**
     * 消息类型
     *
     * @return {@link TopicMessageType}
     */
    TopicMessageType messageType() default TopicMessageType.COMMON;

    /**
     * 是否创建
     */
    boolean created() default true;

    /**
     * 需创建的Topic的描述信息。
     *
     * @return {@link String}
     */
    String remark() default "";
}
