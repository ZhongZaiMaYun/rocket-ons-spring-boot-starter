package com.dm.rocketmq.annotation;

/**
 * 消费模式
 *
 * @author Colin
 * @date 2023/03/14
 */
public enum ConsumeMode {
    /**
     * 并发消费
     */
    CONCURRENTLY,

    /**
     * 顺序消费
     */
    ORDERLY
}
