package com.dm.rocketmq.annotation;


import com.dm.rocketmq.constant.RocketMqConstants;

import java.lang.annotation.*;

/**
 * RocketListener 注解
 *
 * @author Colin
 * @date 2023/04/07
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RocketListener {

    /**
     * topic
     *
     * @return {@link String}
     */
    String topic() default "";

    /**
     * topic 相关信息
     *
     * @return {@link TopicInfo}
     */
    TopicInfo[] topicInfo() default {};

    /**
     * tag
     *
     * @return {@link String}
     */
    String[] tag() default {""};

    /**
     * 是否订阅该 topic 和 tag 默认true 订阅
     *
     * @return boolean
     */
    boolean subscribe() default true;

    /**
     * 消费模式
     *
     * @return {@link ConsumeMode}
     */
    ConsumeMode consumeMode() default ConsumeMode.CONCURRENTLY;


    /**
     * 消费组
     * 默认： RocketMqConstants.DEFAULT_GROUP 默认组
     * 默认情况下：
     * ConsumeMode.CONCURRENTLY 模式 是在 ${rocket-mq.consume.default-config.group-id} 取值
     * ConsumeMode.ORDERLY 模式 是在 ${rocket-mq.order-consume.default-config.group-id} 取值
     *
     * @return {@link String}
     */
    String groupId() default RocketMqConstants.DEFAULT_GROUP;

}
