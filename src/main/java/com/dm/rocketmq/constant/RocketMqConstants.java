package com.dm.rocketmq.constant;

/**
 * mq常量
 *
 * @author Colin
 * @date 2023/03/16
 */
public interface RocketMqConstants {

    /**
     * 默认组
     */
    String DEFAULT_GROUP = "defaultGroup";

    String DEFAULT_INSTANCE_ID = "${rocket-mq.instanceId}";

    String CONSUME_DEFAULT_GROUP = "${rocket-mq.consume.default-config.group-id}";

    String ORDER_CONSUME_DEFAULT_GROUP = "${rocket-mq.order-consume.default-config.group-id}";
}
